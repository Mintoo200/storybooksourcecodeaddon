import React from 'react';

import { Button } from './Button';

export default {
  title: 'With Multiple URL',
  component: Button,
  parameters: {
    componentSource: {
      url: [
        'https://gitlab.com/api/v4/projects/23148581/repository/files/app%2Fviews%2Epy/raw?ref=master',
        'https://gitlab.com/api/v4/projects/23148581/repository/files/app%2Fviews%2Epy/raw?ref=master',        
      ],
      language: 'python',
    }
  },
};

const Template = (args) => <Button {...args} />;

export const WithMultipleURL = Template.bind({});
WithMultipleURL.args = {
  primary: true,
  label: 'Button',
};
