import React from 'react';

import { Button } from './Button';

export default {
  title: 'With Multiple Local Files',
  parameters: {
    componentSource: {
      code: [
        'export default "This is my code"',
        'export default "This is also my code"',
      ],
      language: 'javascript',
    }
  },
  component: Button,
};

const Template = (args) => <Button {...args} />;

export const WithMultipleLocalFiles = Template.bind({});
WithMultipleLocalFiles.args = {
  primary: true,
  label: 'Button',
};
