import React from 'react';

import { Button } from './Button';

export default {
  title: 'With Local File',
  parameters: {
    componentSource: {
      code: 'export default "This is my code"',
      language: 'javascript',
    }
  },
  component: Button,
};

const Template = (args) => <Button {...args} />;

export const WithLocalFile = Template.bind({});
WithLocalFile.args = {
  primary: true,
  label: 'Button',
};
