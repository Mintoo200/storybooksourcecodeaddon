# Storybook-source-code-addon

## What is it?

This is an addon for [Storybook](https://storybook.js.org/). It purpose is to allow anyone navigating your storybook to access the source code. It was implemented as an alternative to regular libraries, and to allow the end user to copy and paste your code instead of using it as a library.

## How do I install it?

It's pretty simple actually:

- First install the dependency:
```sh
$ yarn add -D @storybook-source-code-addon
```
- Then add it to your `.storybook/main.js`
```js
module.exports = {
  addons: ['storybook-source-code-addon']
}
```

## Getting Started

### Using a string

If you happen to have your source code in a string, all you need is to add it to your story:
```js
export default {
  title: 'Your story',
  parameters: {
    componentSource: {
      code: 'export default "This is my code"',
      language: 'javascript',
    }
  },
};
```
With this approach, we recommend using a code file:
```js
// MyComponent.code.js
export default `
const MyComponent = () => "Hello World"
`
```
```js
import MyComponentCode from './MyComponent.code'

export default {
  title: 'Your story',
  parameters: {
    componentSource: {
      code: MyComponentCode,
      language: 'javascript',
    }
  },
};
```

### Using a URL

If your repository is publicly hosted, you can provide a url to your file to the addon:
```js
export default {
  title: 'Your story',
  parameters: {
    componentSource: {
      url: 'https://path.to.your.repository/file%2Etsx',
      language: 'javascript',
    }
  },
};
```
Common providers API include
- `https://gitlab.com/api/v4/projects/<ProjectID>/repository/files/<file path URL encoded>/raw?ref=master` for GitLab

### Multiple files

If you want to expose multiple files, you can do as follow:
```js
export default {
  title: 'Your story',
  parameters: {
    componentSource: {
      url: [
        'https://path.to.your.repository/file%2Etsx',
        'https://path.to.your.repository/file2%2Etsx',        
      ],
      language: 'javascript',
    }
  },
};
```
or
```js
export default {
  title: 'Your story',
  parameters: {
    componentSource: {
      code: [
        'export default "This is my code"',
        'export default "This is also my code"',
      ],
      language: 'javascript',
    }
  },
};
```
